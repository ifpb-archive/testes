const StringUtils = require('./string_utils');

test('Parâmetro nulo gera erro', () => {
  let valor = null;
  let utils = new StringUtils();

  expect(() => utils.reverse(valor).toThrow());
});

test('Parâmetro vazio gera erro', () => {
  let valor = '';
  let utils = new StringUtils();

  expect(() => utils.reverse(valor).toThrow('A string deve ser diferente de nula e não vazia!'));
});

test('Reverter uma string', () => {
  let valor = 'Fulano';
  let utils = new StringUtils();

  expect(() => utils.reverse(valor).toBe('onaluF'));
});