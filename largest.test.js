const Largest = require('./largest');

test('Parâmetro nulo gera erro', () => {
  let numeros = null;
  let largest = new Largest();

  expect(() => largest.getMaior(numeros).toThrow());
});

test('Parâmetro vazio gera erro', () => {
  let numeros = [];
  let largest = new Largest();

  expect(() => largest.getMaior(numeros).toThrow('Parâmetro inválido! Deve ser um array de inteiros com pelo menos um valor!'));
});

test('Buscar o maior número de um array', () => {
  let numeros = [10, 6, 290, 100, 50, 35];
  let largest = new Largest();

  expect(() => largest.getMaior(numeros).toBe(290));
});