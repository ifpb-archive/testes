const TemperatureConverter = require('./temperature');

test('Converter de Fahrenheit para Celsius', () => {
    // Preparar
    let temperatura = 51.3;
    let converter = new TemperatureConverter();

    // Executar
    let resultado = converter.fahrenheitToCelsius(temperatura);

    // Verificar
    expect(resultado).toBeCloseTo(10.72, 2); // Precisão de 2 casas decimais, diferença deve ser menor que 0.005
});

test('Converter de Fahrenheit para Kelvin', () => {
  let temperatura = 32;
  let converter = new TemperatureConverter();

  let resultado = converter.fahrenheitToKelvin(temperatura);

  expect(resultado).toBeCloseTo(273.15, 2);
});

test('Converter de Celsius para Fahrenheit', () => {
  let temperatura = 35.6;
  let converter = new TemperatureConverter();

  let resultado = converter.celsiusToFahrenheit(temperatura);

  expect(resultado).toBeCloseTo(96.08, 2);
});

test('Converter de Celsius para Kelvin', () => {
  let temperatura = 0;
  let converter = new TemperatureConverter();

  let resultado = converter.celsiusToKelvin(temperatura);

  expect(resultado).toBeCloseTo(273.15, 2);
});

test('Converter de Kelvin para Celsius', () => {
  let temperatura = 0;
  let converter = new TemperatureConverter();

  let resultado = converter.kelvinToCelsius(temperatura);

  expect(resultado).toBeCloseTo(-273.15, 2);
});

test('Converter de Kelvin para Fahrenheit', () => {
  let temperatura = 255.4;
  let converter = new TemperatureConverter();

  let resultado = converter.kelvinToFahrenheit(temperatura);

  expect(resultado).toBeCloseTo(0.05, 2);
});