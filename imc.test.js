const IndiceMassaCorporal = require('./imc');

test('Parâmetro negativo gera erro', () => {
  let peso = -56;
  let altura = 153;
  let imc = new IndiceMassaCorporal();

  expect(() => imc.calcular(peso, altura).toThrow());
});

test('Calcular IMC', () => {
  let peso = 56;
  let altura = 153;
  let imc = new IndiceMassaCorporal();

  expect(() => imc.calcular(peso, altura).toBeCloseTo(23.92, 2));
});

test('Obter classificação', () => {
  let resultado = 23.92;
  let imc = new IndiceMassaCorporal();

  expect(() => imc.getClassificacao(resultado).toBe('NORMAL'));
});