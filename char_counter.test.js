const {CharacterCounter, IllegalArgumentError} = require('./char_counter');

test('Caractere aparecendo uma única vez', () => {
    // Preparar
    let frase = 'abcde';
    let caractere = 'a';
    let counter = new CharacterCounter();

    // Executar
    let resultado = counter.contar(frase, caractere);

    // Verificar
    expect(resultado).toBe(1);
});

test('Caractere aparecendo várias vezes', () => {
    // Preparar
    let frase = 'abcadea';
    let caractere = 'a';
    let counter = new CharacterCounter();

    // Executar
    let resultado = counter.contar(frase, caractere);

    // Verificar
    expect(resultado).toBe(3);
});

test('Caractere não aparecendo na frase', () => {
    // Preparar
    let frase = 'bcdef';
    let caractere = 'a';
    let counter = new CharacterCounter();

    // Executar
    let resultado = counter.contar(frase, caractere);

    // Verificar
    expect(resultado).toBe(0);
});

// Documentação sobre casos de teste com exceções em Jest:
// https://jestjs.io/docs/using-matchers#exceptions
test('Parâmetro nulo gera erro', () => {
    // Preparar
    let frase = null;
    let caractere = 'a';
    let counter = new CharacterCounter();

    // Executar e Verificar
    expect(() => counter.contar(frase, caractere)).toThrow(); // qualquer exceção lançada é aceita
    expect(() => counter.contar(frase, caractere)).toThrow(IllegalArgumentError); // especifica o tipo de exceção a ser lançada
    expect(() => counter.contar(frase, caractere)).toThrow('A frase deve ser diferente de nulo e não vazia!'); // especifica a mensagem que deve ser lançada na exceção
    //expect(() => counter.contar(frase, caractere)).toThrow(/diferente de nulo e não vazia/);
});
