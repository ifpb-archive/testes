// Documentação sobre exceções em JavaScript:
// * Lançando exceções:
// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Statements/throw
// * Tratando exceções:
// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Statements/try...catch

class CharacterCounter {

    contar(frase, caractere) {
        if (frase == null || frase.trim() == '') { // trim() remove os espaços em branco do início e/ou fim de um texto.
            throw new IllegalArgumentError('A frase deve ser diferente de nulo e não vazia!');
        }

        let cont = 0;
        for (let c of frase) {
            if (c == caractere) {
				      cont++;
    			  }
        }

        return cont;
    }

}

class IllegalArgumentError extends Error {
  constructor(message) {
    super(message);
  }
}

class RequiredError extends Error {
  constructor(message) {
    super(message);
  }
}

module.exports = {CharacterCounter, IllegalArgumentError, RequiredError};