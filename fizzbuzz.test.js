const FizzBuzz = require('./fizzbuzz');

test('Descrição simples do cenário de teste', () => {
  // Preparar...

  // Executar...

  // Verificar...
});

test('Número divisível por 3', () => {
    // Preparar
    let número = 9;
    let fizzBuzz = new FizzBuzz();

    // Executar
    let resultado = fizzBuzz.calculate(número);

    // Verificar
    expect(resultado).toBe('Fizz');
});

test('Número divisível por 5', () => {
    // Preparar
    let número = 20;
    let fizzBuzz = new FizzBuzz();

    // Executar
    let resultado = fizzBuzz.calculate(número);

    // Verificar
    expect(resultado).toBe('Buzz');
});

test('Número divisível por 3 e 5', () => {
    // Preparar
    let número = 30;
    let fizzBuzz = new FizzBuzz();

    // Executar
    let resultado = fizzBuzz.calculate(número);

    // Verificar
    expect(resultado).toBe('FizzBuzz');
});

test('Número NÃO divisível por 3 e nem por 5', () => {
    // Preparar
    let número = 11;
    let fizzBuzz = new FizzBuzz();

    // Executar
    let resultado = fizzBuzz.calculate(número);

    // Verificar
    expect(resultado).toBe('11');
});
