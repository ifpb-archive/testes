class IndiceMassaCorporal {
    
    calcular(pesoEmKg, alturaEmCm) {
        if (pesoEmKg < 0.0) {
            throw new Error(`O valor do peso deve ser maior que zero: ${pesoEmKg.toFixed(2)}`);
        }
        if (alturaEmCm < 0) {
            throw new Error(`O valor da altura deve ser maior que zero: ${alturaEmCm}`);
        }

        let alturaEmM = alturaEmCm / 100.0;
        let resultado = pesoEmKg / (alturaEmM * alturaEmM);
        return resultado;
    }

    getClassificacao(imc) {
		  if (imc <= 18.4) {
  			return 'ABAIXO';
		  } else if (imc > 18.4 && imc <= 24.9) {
  			return 'NORMAL';
		  } else if (imc > 24.9 && imc <= 29.9) {
  			return 'SOBREPESO';
		  } else if (imc > 29.9 && imc <= 34.9) {
  			return 'OBESIDADE_GRAU_I';
		  } else if (imc > 34.9 && imc <= 39.9) {
  			return 'OBESIDADE_GRAU_II';
		  } 
		
		  return 'OBESIDADE_GRAU_III';
    }

}

module.exports = IndiceMassaCorporal;